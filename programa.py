def es_par(n):
    return(n%2==0)

def es_primo(n):
    i=2
    while(i<n):
        if (n%i==0):
            return False
        i=i+1    
    return True

def imprimir_secuencia(n):

    i=1
    if(n<50):        
        while(i<n*10):
            if(i%n!=0):
                print(i)
            i=i+1        
    else:        
        while(i<=n*5):
            print(i)        
            i=i+1
    return True

def imprimir_tabla(n):

    for i in range(1,51):
        print(i,"X",n, "=",i*n)
    return True


def solucionar():
    
    n=int(input("Ingrese un número"))
    
    if(es_par(n)):
        print("Es Par")
    else:
        print("No Es Par")

    if(es_primo(n)):
        print("Es Primo")
    else:
        print("No Es Primo")
    print("Imprimiendo secuencia...")
    imprimir_secuencia(n)
    print("Imprimiendo Tabla...")
    imprimir_tabla(n)

solucionar()


